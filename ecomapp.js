//Dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

//access to routes
const userRoutes = require("./routes/userRoutes")
const productRoutes = require("./routes/productRoutes")
const orderRoutes = require("./routes/orderRoutes")

//Server creation
const app = express()
const port = 4000

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended:true}))

mongoose.connect("mongodb+srv://eltonUser:chicharon@cluster0.7jgiw.mongodb.net/b170-course-booking?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})
let db=mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open",() => console.log("We're connected to the database"))

app.use("/api/users", userRoutes);
app.use("/api/products", productRoutes);
app.use("/api/orders", orderRoutes);

app.listen(port,() => console.log(`API now online at port ${port}`))

