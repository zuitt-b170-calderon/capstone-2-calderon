const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const productController = require("../controllers/productControllers.js")


// retrieve all active products

router.get("/active", (req, res) => {
	productController.getActiveProducts().then(resultFromController => res.send(resultFromController))
})

// retrieve single product

router.get("/:productId",  (req, res) => {
	console.log(req.params.productId);
	productController.getProduct(req.params.productId).then(result => res.send(result))
})

//create product (Admin only)

router.post("/", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization) 
    productController.addProduct(req.body, userData).then(resultFromController => res.send(resultFromController))
})

//update product information (Admin only)

router.put("/:productId", auth.verify, (req, res) => {
	productController.updateProduct(req.params, req.body).then(result => res.send(result))
})




module.exports = router