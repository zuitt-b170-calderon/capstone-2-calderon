const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const userController = require("../controllers/userControllers.js")

//User Registration

router.post("/register", ( req, res ) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

//User Authentication

router.post("/login", ( req, res ) => {
	userController.userAuthentication(req.body).then(resultFromController => res.send(resultFromController))
})

//Set user Admin (Admin Only)

router.post("/", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization) 
    userController.userAdminUpdate(req.body, userData).then(resultFromController => res.send(resultFromController))
})







module.exports = router