const Product = require("../models/Product.js")
const User = require("../models/User.js")
const Order = require("../models/Order.js")

// retrieve active products
module.exports.getActiveProducts = () => {
	return Product.find( { isActive: true } ).then((result,error) => {
		if (error) {
			console.log(error)
		}else{
			return result
		}
	})
}

//retrieve single product

module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams).then(result => {
		return result
	})
}

//create product (Admin only)

module.exports.addProduct = (reqBody, userData) => {

    return User.findById(userData.userId).then(result => {

        if (userData.isAdmin === false) {
            return "You are not an admin"
        } else {
            let newProduct = new Product({
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
            })
        
            return newProduct.save().then((product, error) => {
                if(error) {
                    return false
                } else {
                    return "Product is created successfully"
                }
            })
        }
        
    });    
}

//update product information (Admin only)

module.exports.updateProduct = ( reqParams, reqBody ) => {
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((result, error) => {
		if (error) {
			return false
		} else	{
			return true
		}
	})
}

//archive product (Admin only)

module.exports.archivedCourse=(reqParams)=>{
	let updatedCourse ={
		is Active: false
	}
}
