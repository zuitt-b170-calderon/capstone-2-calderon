const mongoose = require("mongoose")

const orderSchema = new mongoose.Schema({
	totalAmount:{
		type: Number,
		required: [true, "Amount is required"]
	},
	userId:{
		type: String,
		required: [true,"User Id is required"]
	},
	purchases:[
	{
		productId:{
			type: String,
			required: [true,"Product Id is required"]
		},
		price:{
			type: Number,
			required: [true, "Price of product is required"]
		}
		createdOn:{
			type: Date,
			default: new Date()
		},
	}
	] 
})

module.exports = mongoose.model("Orders", orderSchema);