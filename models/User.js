const mongoose = require("mongoose")

const userSchema = new mongoose.Schema({
	firstName:{
		type: String,
		required: [true, "First name is required"]
	},
	lastName:{
		type: String,
		required:[true,"Last name is required"]
	},
	email:{
		type: String,
		required: [true,"eMail is required"]
	},
	password:{
		type: String,
		required: [true,"Password is required"]
	},
	isAdmin:{
		type: Boolean,
		default: false
	},
	purchases:[
	{
		productId:{
			type: String,
			required: [true,"Product Id is required"]
		},
		createdOn:{
			type: Date,
			default: new Date()
		},
		status:{
			type: String,
			default: "Paid"
		}
	}
	] 
})

module.exports = mongoose.model("User", userSchema);
